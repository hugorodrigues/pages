// SPDX-FileCopyrightText: 2022 Hugo Rodrigues
//
// SPDX-License-Identifier: MIT

"use strict";

Array.prototype.random = function () {
    return this[Math.floor((Math.random()*this.length))];
}

// From fortune
const MESSAGES = [
    "Geek since 1995!", // Me
    "Geek, gamer and father.", // Me
    "Unix is user friendly. It just happens to be very selective about who it decides to make friends with.", // Unknown
    "The sooner all the animals are extinct, the sooner we'll find their money.", // Ed Bluestone
    "The course of true anything never does run smooth.", // Samuel Butler
    'echo "Congratulations.  You aren\'t running Eunice."', // Larry Wall in Configure from the perl distribution
    "Nothing is so firmly believed as that which we least know.", // Michel de Montaigne
    '"We are on the verge: Today our program proved Fermat\'s next-to-last theorem."', // Epigrams in Programming, ACM SIGPLAN Sept. 1982
    "There's an old proverb that says just about whatever you want it to.", // Unknown
    "VMS is like a nightmare about RXS-11M.", // Unknown
    "Deadwood, n.: Anyone in your company who is more senior than you are.", // Unknown
];

const TIMEOUT_START = 1000;
const TIMEOUT_TYPE_CHAR = 50;
const TIMEOUT_START_DELETE = 10000;

function write_message(message, i) {
    // DOM object used for the message
    var message_object = document.getElementById("message");

    // Add new char to the message
    if (i < message.length) {
        message_object.innerHTML += message.charAt(i);
        setTimeout(write_message, TIMEOUT_TYPE_CHAR, message, i + 1);
    } else { // Message is complete, start deletion
        if (MESSAGES.length == 1) {
            // MESSAGES only has one message, so there's nothing to remove
            console.warn("Refusing to delete and retype the same message");
        }
        else if ( i == message.length) { // Message his complete, lets wait TIMEOUT_START_DELETE to start the actual deletion
            setTimeout(write_message, TIMEOUT_START_DELETE, message, message.length + 1);
        } else { // Actual deletion
            var current = message_object.innerHTML;
            // Remove last char
            if (current.length > 0) {
                message_object.innerHTML = current.substring(0, current.length - 1);
                setTimeout(write_message, TIMEOUT_TYPE_CHAR, message, i);
            } else {
                // Fetch new message and wait TIMEOUT_START seconds to start typing again
                var new_message = message;
                while (new_message == message) {
                    new_message = MESSAGES.random();
                }
                setTimeout(write_message, TIMEOUT_START, new_message, 0);
            }
        }
    }
}

window.onload = function () {
    setTimeout(function () {
        document.getElementById("message").nextSibling.classList.remove("hidden");
        write_message(MESSAGES.random(), 0);
    }, TIMEOUT_START);
}
